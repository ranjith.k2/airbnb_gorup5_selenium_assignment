package com.airbnb;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.google.common.io.Files;

import Page_object.Login;
import Page_object.SearchHotel_page;
import Page_object.payement_page;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Airbnb_classs {

	public static void main(String[] args) throws AWTException, InterruptedException, BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.airbnb.co.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		Login l = PageFactory.initElements(driver, Login.class);
		l.clicklog();

		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		l.clickcontinue();
		File fs = new File("/home/ranjith/Documents/AIr/Airbnp_Data.xls");
		FileInputStream input = new FileInputStream(fs);
		Workbook book = Workbook.getWorkbook(input);
		Sheet sheet = book.getSheet("Sheet1");
		int rows = sheet.getRows();
		int columns = sheet.getColumns();
		String firstname = sheet.getCell(0, 0).getContents();
		String lastname = sheet.getCell(0, 1).getContents();
		String mai = sheet.getCell(0, 2).getContents();
		String passwords = sheet.getCell(0, 3).getContents();
		l.sendemail(lastname);
		l.clickeailbutton();
		l.sendpass(mai);
		l.clickpassbutton();
		Thread.sleep(3000);
		SearchHotel_page s = PageFactory.initElements(driver, SearchHotel_page.class);
		s.Filterhotel();
		Thread.sleep(3000);
		s.clickhotel();

		String windowHandle = driver.getWindowHandle();
		System.out.println(windowHandle);

		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println(windowHandles);

		List<String> ref = new ArrayList<String>(windowHandles);
		driver.switchTo().window(ref.get(1));

		payement_page p = PageFactory.initElements(driver, payement_page.class);
		Thread.sleep(3000);
		p.Resverbutton();
		Thread.sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		p.clickcard();

		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_ENTER);
		p.sendfedback(passwords);
		Thread.sleep(2000);
		// driver.findElement(By.xpath("//span[@class=\"tjxdvlu dir
		// dir-ltr\"]")).click();
		p.resverclick();
       TakesScreenshot ts=(TakesScreenshot)driver;
       File screenshotAs = ts.getScreenshotAs(OutputType.FILE);
       File f=new File("/home/ranjith/eclipse-workspace/Airbnb/src/test/resources/screenshot/payment.png");
       Files.copy(screenshotAs, f);
	}

	
}
