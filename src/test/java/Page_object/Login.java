package Page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login {
	 WebDriver driver;

	public Login(WebDriver driver) {
		this.driver=driver;
	}

	@FindBy(xpath = "//button[@type='button'] [@class='c1grjlav crawnjq dir dir-ltr']")
	private WebElement clicklogin;

	public void clicklog() {
		clicklogin.click();

	}
	
	@FindBy(xpath = "//div[text()='Continue with email']")
	private WebElement continuemail;
	public void clickcontinue() {
		continuemail.click();
	}

	@FindBy(xpath = "\"//button[@data-testid='social-auth-button-email']\"")
	private WebElement clickemail;

	public void clickemilbutton() {
		clickemail.click();
	}

	@FindBy(name = "user[email]")
	private WebElement enteremail;

	public void sendemail(String email) {
		enteremail.sendKeys(email);
	}
   
	@FindBy(xpath = "//span[@class='_kaq6tx']")
	private WebElement clickemailbutton;
	public void clickeailbutton() {
		clickemailbutton.click();
	}
	
	@FindBy(name = "user[password]")
	private WebElement enterpass;
	public void sendpass(String pass) {
		enterpass.sendKeys(pass);
	}
	@FindBy(xpath = "//span[@class='_kaq6tx']")
	private WebElement clickpass;
	public void clickpassbutton() {
		clickpass.click();
	}
}
