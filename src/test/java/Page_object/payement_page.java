package Page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class payement_page {
	WebDriver driver;

	public payement_page(WebDriver driver) {

		this.driver = driver;

	}

	@FindBy(xpath = "(//span[@class=\"t12u7nq4 dir dir-ltr\"])[2]")
	private WebElement clickResverbutton;

	public void Resverbutton() {
		clickResverbutton.click();
	}

	@FindBy(xpath = "/html/body/div[9]/section/div/div/div[2]/div/div[1]/button/span")

	private WebElement x;

	public void clickX() {
		x.click();
	}

	@FindBy(xpath = "//button[text()='Add']")
	private WebElement Add;

	public void clickaddbutton() {
		Add.click();
	}

	@FindBy(id = "phone_number")
	private WebElement phonenumber;

	public void enterPhonenumber(String number) {
		phonenumber.sendKeys(number);
	}

	@FindBy(xpath = "//button[text()='Continue']")
	private WebElement continu;

	public void clickcontinue() {
		continu.click();
	}

	@FindBy(id = "null_textArea")
	private WebElement addmsg;

	public void sendmesg(String msg) {
		addmsg.sendKeys(msg);

	}

	@FindBy(xpath = "//button[text()='Save']")
	private WebElement save;

	public void savemsg() {
		save.click();
	}

	@FindBy(xpath = "//button[text()='Add']")
	private WebElement Adds;

	public void clickaddbutton2() {
		Adds.click();
	}

	@FindBy(id = "dropdown-selector-payment_option_selector-input")
	private WebElement card;

	public void clickcard() {
		card.click();
	}

	@FindBy(id = "null_textArea")
	private WebElement fed;

	public void sendfedback(String feds) {
		fed.sendKeys(feds);
	}
    @FindBy(xpath = "//span[@class=\"tjxdvlu dir dir-ltr\"]")
    private WebElement res;
    public void resverclick() {
    	res.click();
	}
}
