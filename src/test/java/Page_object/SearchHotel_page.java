package Page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchHotel_page {

	WebDriver driver;

	public SearchHotel_page(WebDriver driver) {
  this.driver=driver;
	}

	@FindBy(xpath = "(//img[@class=\"i1wps9q8 dir dir-ltr\"])[3]")
	private WebElement clickfavHotel;

	public void Filterhotel() {
		clickfavHotel.click();
	}

	@FindBy(xpath = "(//img[@class=\"_6tbg2q\"])[3]")
	private WebElement clickdisplyed;

	public void clickhotel() {
		clickdisplyed.click();
	}

}
